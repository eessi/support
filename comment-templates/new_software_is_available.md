**Name**

software installation ready

**content**


Dear __USER__,

The latest version of __SOFTWARE__ is now installed in EESSI, available via:

module load __SOFTWARE__/__MODULENAME__

Don't hesitate to contact us via support@eessi.io or open a GitLab issue on our service desk in case of problems or questions regarding EESSI.

Kind regards,

the EESSI team
