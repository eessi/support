**Name**

software install received

**content**

Dear __USER__, 

Your software installation request for __FILL_OUT_SOFTWARE__ has
been received.

We will do what we can to look into this request as soon as possible.

Please keep in mind that we may have a backlog of other installation requests to process first, and that you may need to exercise some patience before we get to your request.

Don't hesitate to reply to this message to ask for an update if you don't hear back from us for a while, or in case this is blocking your work.

For other questions or problems related to EESSI infrastructure, please send us a message via support@eessi.io or open a GitLab issue on our service desk, https://gitlab.com/eessi/support. (Start a new thread for this, please don't recycle existing tickets).

Regards, 

the EESSI team
