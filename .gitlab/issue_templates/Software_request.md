# Software installation request

## Personal details

Institution (optional):

## Detail of software

Software name (required):

Availability of software:
- [ ] Is it a software update for a software that is already available in EESSI?
- [ ] Is the software already supported in [EasyBuild](https://docs.easybuild.io/version-specific/supported-software/)?
- [ ] None of the above

Website of software (required):

[SPDX identifier for software license](https://spdx.org/licenses/) (Required, must be open source): 

Required software version (a specific version, or a description like 'latest'): 

Dependencies (both required and optional ones that are required for your use case):

Pointer to installation guide (Required):

Pointer to documentation on how to test installation:

Instructions to run test case:

When would you like to use this software? (Required):
- [ ] Whenever it's available (lowest priority)
- [ ] Sometime in the coming weeks/months is fine (low priority)
- [ ] In the coming days/weeks would be nice (medium priority)
- [ ] ASAP, this is blocking my work (high priority)

List of already installed modules you want to use together with this software:

List of software that is not available yet in EESSI that is also required:

Additional comments:





