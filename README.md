# **EESSI support portal**

<p align="center">
  <img width="30%" src="img/logos/multixscale_logo.png" alt="MustiXscale logo">
  <img width="30%" src="img/logos/EESSI_logo_horizontal_transparant.png" alt="MustiXscale logo">
</p>

Thanks to the [MultiXscale EuroHPC project](https://www.multixscale.eu) we are able to provide support to the users of EESSI.


## Contact
***

### **Create an issue with your GitLab account**
If you have a GitLab account or create one you can create and manage your issue on GitLab. You can also use one of our issue templates.

### **Contact us via E-mail**
If you do not have a GitLab account you can also ask for support via E-mail.
<!-- Add text where we say what the neccesary things are that they need to add to their request-->
<!--Ask for help-->
<!--for software requests-->
<!--Security issues-->
<!--reporting bugs-->
Mail us at `support (at) eessi.io`.

<br>

## Reporting a problem
***
If you run into any problems while using EESSI or you find a bug. Please send them to us. 

You can use the following template: 

* [*Report problem*](https://gitlab.com/eessi/support/-/issues/new?issuable_template=Report_problem)

Or send us an e-mail. Please specify the following things in your e-mail:
* a clear description of the problem, incl. error messages and/or warnings you are seeing
* which EESSI version you are using
* which system you are working on, incl. the CPU architecture of that system
* instructions on how to reproduce the problem

<br>

## Software installation requests
***

Would you like to make a software installation request?

Create an issue with the following template:
* [*Software request*](https://gitlab.com/eessi/support/-/issues/new?issuable_template=Software_request)

Or send us an e-mail. Please specify the following things in your e-mail:
* name and version of the software
* software website
* installation instructions
* information on how to test the installation

<br>

## Getting help
***

Other Questions? 
[Consult our Documentation](http://www.eessi.io/docs/) on using EESSI. 

<!--
Should we recomend that the user search the existing issues first / give a link for them?
-->  

Create an issue with the following template:

* [*General question*](https://gitlab.com/eessi/support/-/issues/new?issuable_template=Question) 

Or send us an e-mail. Please include the following things in your e-mail:
* Your affiliation (university, research institute, company, ...)
* A clear description of your question
* If relevant: which version of EESSI you are using, which system you are using (incl. the CPU architecture of that system), ...

<!--
## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->

<!--
## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
-->

<!--
## License
For open source projects, say how it is licensed.
-->

<!--
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->

<br>

***

Funded by the European Union. This work has received funding from the European High Performance Computing Joint Undertaking (JU) and countries participating in the project under grant agreement No 101093169.
